<?php

namespace frontend\models;

use phpDocumentor\Reflection\Types\Integer;
use Yii;

class Test
{

    /**
     * @param int $max
     * @return array
     */
    public static function getNewsList($max = 1)
    {
        $max = intval($max);
        $sql = 'SELECT * FROM news LIMIT '. $max;

        $result = Yii::$app->db->createCommand($sql)->queryAll();

        if (!empty($result) && is_array($result)) {
            foreach ($result as &$item) {
                $item['content'] = Yii::$app->stringHelper->getShort($item['content'], 50, '');
            }
        }

        return $result;
    }

    /**
     * @param int $id
     * @return array|false
     */
    public static function getItem($id)
    {
        $id = intval($id);
        $sql = "SELECT * FROM news WHERE id = $id";

        return Yii::$app->db->createCommand($sql)->queryOne();
    }

}