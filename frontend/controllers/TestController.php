<?php


namespace frontend\controllers;
use Yii;
use phpDocumentor\Reflection\DocBlock\Tags\Param;
use yii\web\Controller;
use frontend\models\Test;


class TestController extends Controller
{
    public function actionIndex()
    {
        $max = Yii::$app->params['maxNewsPostLimit'];
        $list = Test::getNewsList($max);

        return $this->render('index', [
            'list'=> $list,
        ]);
    }

    public function actionView($id)
    {
        $item = Test::getItem($id);

        return $this->render(
            'view', [
                'item' => $item,
            ]
        );
    }

}