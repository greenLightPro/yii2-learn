<?php


namespace frontend\controllers;
use Yii;
use yii\web\Controller;
use frontend\models\News;



class CountController extends Controller
{
    public function actionIndex()
    {
        $news = News::getNewsList();
        $count =  count($news);

        return $this->render('index', [
            'count' => $count,
        ]);
    }

}