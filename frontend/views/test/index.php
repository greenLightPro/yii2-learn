<p><?php echo Yii::$app->urlManager->createUrl(['test/index', 'id' => 5]); ?></p>
<p><?php echo Yii::$app->urlManager->createAbsoluteUrl(['test/index']); ?></p>


<?php



foreach ($list as $item): ?>
    <ul>
        <li>
            <h3><a href="<?= Yii::$app->urlManager->createAbsoluteUrl(['test/index']) . '/' . $item['id']; ?>"><?= $item['title']; ?></a></h3>
            <p><?= $item['content']; ?></p>
            <hr>
        </li>
    </ul>
<?php endforeach; ?>
