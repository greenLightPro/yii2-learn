<?php

namespace frontend\components;

use Yii;

class StringHelper
{
    private $limit;

    public function __construct()
    {
        $this->limit = Yii::$app->params['shortTextLimit'];
    }

    public function getShort($string, $limit = null, $tail = '...')
    {
        if ($limit === null) {
            $limit = $this->limit;
        } else {
            $limit = intval($limit);
        }

        $tail = strval($tail);
        $shortString = substr($string, 0, $limit);

        if (strlen($string) > $limit) {
            return ($shortString . $tail);
        }
        return  $shortString;
    }

}